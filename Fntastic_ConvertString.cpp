﻿// Fntastic_ConvertString.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <bitset>

typedef unsigned char uchar_t;

using std::cout;
using std::cin;
using std::endl;

uchar_t* stringToBrackets(uchar_t* string)
{
    uint8_t array[256];
    memset(&array, 0, 256);
    
    for (uchar_t* a = string; *a != '\0'; a++)
        array[*a]++;

    for (uchar_t* a = string; *a != '\0'; a++)
        *a = array[*a] > 1 ? ')' : '(';

    return string;
}


int main()
{
    uchar_t string[] = "Let's imagine ...You're watching TV. It's a hot evening: You feel thirsty. You see an advert for a refreshing drink. You see people looking cool and relaxed. You notice the name of the refreshing drink because you think it could be useful for you to satisfy your thirst.";
    cout << stringToBrackets(reinterpret_cast<uchar_t*>(&string));
}
